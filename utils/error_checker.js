exports.checkDuplicateError = function (error) {
    try {
        if (!error) return false;
        const {parent: sqlMessage} = error;
        return sqlMessage.toString().toLowerCase().includes("duplicate");
    } catch (e) {
        return false;
    }
}
