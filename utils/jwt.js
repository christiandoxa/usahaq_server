const jwt = require('jsonwebtoken');
const {jwtKey} = require("../keys");

exports.createToken = function (payload) {
    return jwt.sign(payload.toJSON(), jwtKey, {expiresIn: '1h'});
}

exports.parseToken = function (token) {
    return jwt.verify(token, jwtKey);
}
