const fs = require("fs");

exports.checkFileExist = function (filePath) {
    try {
        return fs.existsSync(filePath);
    } catch (err) {
        throw err;
    }
}
