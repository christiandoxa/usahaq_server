exports.divideMachineLearningArray = function (value, divider) {
    let arr = [];
    for (let i = 0; i < value.length; i++) {
        arr.push([value[i] / divider]);
    }
    return arr;
};

exports.multiplyMachineLearningArray = function (value, multiplier) {
    let arr = [];
    for (let i = 0; i < value[0].length; i++) {
        arr.push(value[0][i] * multiplier);
    }
    return arr;
}

exports.findMean = function (value) {
    let temp = 0;
    for (let i = 0; i < value.length; i++) {
        temp += value[i];
    }
    return temp / value.length;
}
