module.exports = function (dob) {
    try {
        const dateOfBirth = new Date(dob);
        const diff_ms = Date.now() - dateOfBirth.getTime();
        const age_dt = new Date(diff_ms);
        return Math.abs(age_dt.getUTCFullYear() - 1970);
    } catch (e) {
        throw new Error("Can't process date");
    }
}
