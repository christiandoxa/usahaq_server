const {exec} = require('child_process');
const CronJob = require('cron').CronJob;

module.exports = new CronJob('00 00 00 * * *', function () {
    exec('rm -r ./tmp/*.xls')
}, null, true, 'Asia/Jakarta');
