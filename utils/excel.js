// noinspection HttpUrlsUsage

const fs = require('fs');
const axios = require('axios');
const qs = require('qs');
const XLSX = require('xlsx');
const path = require('path');
const {getCurrentDateAndPast} = require('./date');
const {checkFileExist} = require('./exist');
const {filterJSON} = require('../constants');
const cwd = path.join(__dirname, '..');
const config = {
    method: 'post',
    url: 'http://hargapangan.id/tabel-harga/pedagang-besar/komoditas',
    responseType: 'arraybuffer',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
    },
};
const baseData = {
    'format': 'xls',
    'price_type_id': '3',
    'filter_province_ids': '16',
    'filter_regency_ids': '46',
    'filter_show_regency': '1',
    'filter_show_market': '1'
};

async function getExcel(type) {
    let filter;
    if (filterJSON[type]) {
        filter = filterJSON[type];
    } else {
        throw new Error("Type not found");
    }
    const {currentDate, pastDate} = getCurrentDateAndPast();
    return new Promise(function (resolve, reject) {
        baseData['filter_commodity_id'] = filter;
        baseData['filter_start_date'] = pastDate;
        baseData['filter_end_date'] = currentDate;
        config.data = qs.stringify(baseData);
        processRequest(resolve, reject, filter + currentDate);
    })
}

function processRequest(resolve, reject, name) {
    const fileName = name + '.xls';
    if (checkFileExist(path.join(cwd, 'tmp', fileName))) return resolve(fileName);
    axios(config)
        .then(function (response) {
            fs.writeFile(path.join(cwd, 'tmp', fileName), response.data, function (err) {
                if (err) return reject(err);
                return resolve(fileName);
            });
        })
        .catch(function (error) {
            return reject(error);
        });
}

function getExcelValueForML(fileName) {
    const workbook = XLSX.readFile(path.join(cwd, 'tmp', fileName));
    const first_sheet_name = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[first_sheet_name];
    const rowObject = XLSX.utils.sheet_to_json(worksheet)[7];
    let arr = [];
    Object.keys(rowObject).forEach(function (key) {
        if (!isNaN(rowObject[key])) {
            arr.push(rowObject[key]);
        }
    });
    return arr;
}

exports.getExcelAndValue = async function (type) {
    const filename = await getExcel(type);
    return getExcelValueForML(filename);
};
