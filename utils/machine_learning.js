const tf = require('@tensorflow/tfjs-node');
const path = require('path');
const cwd = path.join(__dirname, '..');

let model = {
    chilli: null,
    meat: null,
    chicken: null,
    onion: null,
    big_red_chilli: null,
    garlic: null,
    rice: null,
    sugar: null,
    oil: null,
    egg: null,
    chicken_sales: null
};

exports.getModel = async function (type) {
    let modelName;
    let selectedModel;
    if (model[type] !== undefined) {
        modelName = `${type}_model`;
        selectedModel = model[type];
    } else {
        throw new Error("Model not found");
    }
    if (selectedModel) {
        return selectedModel;
    }
    const modelLocation = path.join(cwd, 'ml_models', modelName);
    selectedModel = await tf.node.loadSavedModel(modelLocation);
    return selectedModel;
}
