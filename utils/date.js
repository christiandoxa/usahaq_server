function dateFormat(d) {
    let date = d.getDate().toString();
    let month = (d.getMonth() + 1).toString();
    if (date.length === 1) {
        date = `0${date}`
    }
    if (month.length === 1) {
        month = `0${month}`
    }
    return date + "-" + month + "-" + d.getFullYear();
}

exports.getCurrentDateAndPast = function () {
    const d = new Date();
    d.setDate(d.getDate() - 30);
    return {
        currentDate: dateFormat(new Date()),
        pastDate: dateFormat(d)
    }
};
