const path = require('path');
const {deleteLocalFile} = require("./file");
const {checkFileExist} = require("./exist");
const {admin} = require('../keys');
const bucket = admin.storage().bucket();
const cwd = path.join(__dirname, '..');

class CloudStorage {
    static async cloudStorageUpload(file, folder) {
        try {
            if (!file) {
                return;
            }
            const filePath = file.path;
            const fileName = path.basename(filePath);
            await bucket.upload(filePath, {
                destination: path.join(folder, fileName)
            });
        } catch (e) {
            deleteLocalFile(path.basename(file.path));
            throw e;
        }
    }

    static async cloudStorageDownload(fileName, folder) {
        try {
            const filePath = path.join(cwd, 'files', fileName);
            if (checkFileExist(filePath)) {
                return;
            }
            return await bucket.file(path.join(folder, fileName)).download({
                destination: filePath
            });
        } catch (e) {
            deleteLocalFile(fileName);
            throw e;
        }
    }
}

module.exports = CloudStorage;
