const path = require('path');
const fs = require('fs');
const cwd = path.join(__dirname, '..');

exports.deleteLocalFile = function (file, location = 'files') {
    if (!file) return;
    let fullPath;
    if (typeof file === "object") {
        fullPath = file.path;
    } else {
        fullPath = path.join(cwd, location, file);
    }
    return fs.unlink(fullPath, function (err) {
        if (err) {
            console.log(err);
        }
    });
}
