const tf = require('@tensorflow/tfjs-node');
const {OK, getReasonPhrase} = require("http-status-codes");
const {internalServerError} = require('./common_response');
const {
    array: {divideMachineLearningArray, multiplyMachineLearningArray, findMean},
    machineLearning: {getModel},
    excel: {getExcelAndValue},
    random: {getRandomInt}
} = require('../utils');
const {maxPriceJSON} = require('../constants');

function predictionResponse(result, res) {
    return res.status(OK).json({
        status: OK,
        message: getReasonPhrase(OK),
        result
    });
}

async function commonProcess({value, param_type, maxPrice, res, getNextDay = false}) {
    const input = divideMachineLearningArray(value, maxPrice);
    const model = await getModel(param_type);
    const outputTensor = model.predict(tf.tensor([input]));
    const output = multiplyMachineLearningArray(await outputTensor.array(), maxPrice);
    const finalOutput = findMean(output)
    if (getNextDay) {
        return getNextDayPrediction({input, finalOutput, model, max_price: maxPrice, res});
    }
    return predictionResponse(finalOutput, res);
}

async function getNextDayPrediction({input, finalOutput, model, max_price, res}) {
    const result = []
    result.push(finalOutput)
    let outputResult = finalOutput
    for (let i = 0; i < 10; i++) {
        input.push([outputResult / max_price]);
        const outputTensor = model.predict(tf.tensor([input]));
        const output = multiplyMachineLearningArray(await outputTensor.array(), max_price);
        outputResult = findMean(output);
        result.push(outputResult)
    }
    return predictionResponse(result, res);
}

function getMaxPrice(paramType) {
    if (maxPriceJSON[paramType]) {
        return maxPriceJSON[paramType];
    } else {
        throw new Error("Type not found");
    }
}

async function processChickenSales(param_type, res) {
    const randomSales = [];
    for (let i = 0; i < 30; i++) {
        randomSales.push(getRandomInt(60, 80));
    }
    return await commonProcess({value: randomSales, param_type, maxPrice: 1, res, getNextDay: true});
}

class MachineLearning {
    async currentPrediction(req, res) {
        try {
            const {param_type} = req.params;
            if (param_type === 'chicken_sales') return await processChickenSales(param_type, res);
            const maxPrice = getMaxPrice(param_type);
            const value = await getExcelAndValue(param_type);
            return await commonProcess({value, param_type, maxPrice, res, getNextDay: true});
        } catch (e) {
            return internalServerError(e, res);
        }
    }

    async getPrediction(req, res) {
        try {
            const {param_type} = req.params;
            const maxPrice = getMaxPrice(param_type);
            const {payload} = req.body;
            return await commonProcess({value: payload, param_type, maxPrice, res});
        } catch (e) {
            return internalServerError(e, res);
        }
    }
}

module.exports = new MachineLearning();
