const {OK, NOT_FOUND, CONFLICT, INTERNAL_SERVER_ERROR, getReasonPhrase} = require("http-status-codes");
const {message: {ACCOUNT_NOT_FOUND}} = require("../constants");
const {
    errorChecker: {checkDuplicateError},
    jwt: {createToken}
} = require("../utils");

exports.notFound = function (res) {
    return res.status(NOT_FOUND).json({
        "status": NOT_FOUND,
        "message": getReasonPhrase(NOT_FOUND)
    });
};

exports.accountResponse = function (payload, res) {
    return res.status(OK).json({
        status: OK,
        message: getReasonPhrase(OK),
        accessToken: createToken(payload),
        account: payload
    });
}

exports.accountNotFound = function (res) {
    return res.status(NOT_FOUND).json({
        status: NOT_FOUND,
        message: ACCOUNT_NOT_FOUND
    });
}

exports.internalServerError = function (error, res) {
    return res.status(INTERNAL_SERVER_ERROR).json({
        status: INTERNAL_SERVER_ERROR,
        message: getReasonPhrase(INTERNAL_SERVER_ERROR),
        error: error.toString()
    });
}

exports.handleDuplicateError = function (error, res) {
    const isDuplicate = checkDuplicateError(error);
    return res.status(isDuplicate ? CONFLICT : INTERNAL_SERVER_ERROR).json({
        status: isDuplicate ? CONFLICT : INTERNAL_SERVER_ERROR,
        message: isDuplicate ? getReasonPhrase(CONFLICT) : getReasonPhrase(INTERNAL_SERVER_ERROR),
    });
}
