const {sequelize, BUSINESS, PRODUCT, TRANSACTION, DETAIL_TRANSACTION, PAYMENT_METHOD} = require('../models');
const {internalServerError} = require('./common_response');
const {BAD_REQUEST, OK, getReasonPhrase} = require('http-status-codes');

function transactionIsNotInTheSameType(res) {
    return res.status(BAD_REQUEST).json({
        status: BAD_REQUEST,
        message: "Transaction isn't in the same type"
    })
}

function processProduct(product, isSingleTransaction) {
    if (!product) throw new Error("Product not found");
    if (isSingleTransaction) return {is_same_type: true, product};
    if (product.length === 0) throw new Error("Product not found");
    if (product.length === 1) throw new Error("Using the same product id");
    let previousType = null;
    for (let i = 0; i < product.length; i++) {
        if (i !== 0 && previousType !== product[i].IS_SELL)
            return {is_same_type: false};
        previousType = product[i].IS_SELL;
    }
    return {is_same_type: true, product};
}

async function checkTransactionAndGetProduct(id_product, t) {
    const isSingleTransaction = !Array.isArray(id_product);
    const product = isSingleTransaction ? await PRODUCT.findOne({
        where: {ID_PRODUCT: id_product},
        transaction: t
    }) : await PRODUCT.findAll({
        where: {ID_PRODUCT: id_product},
        transaction: t
    });
    return processProduct(product, isSingleTransaction);
}

function calculateTotalAndQuantity(product, quantity) {
    let total = 0;
    let sumQuantity = 0;
    if (Array.isArray(product)) {
        for (let i = 0; i < product.length; i++) {
            const intQuantity = parseInt(quantity[i]);
            total += product[i].PRICE * intQuantity;
            sumQuantity += intQuantity;
        }
    } else {
        const intQuantity = parseInt(quantity)
        total += product.PRICE * intQuantity;
        sumQuantity = intQuantity;
    }
    return {total, sumQuantity};
}

async function processDetailTransaction({idTransaction, product, quantity, note, t}) {
    if (Array.isArray(product)) {
        for (let i = 0; i < product.length; i++) {
            await DETAIL_TRANSACTION.create({
                ID_TRANSACTION: idTransaction,
                ID_PRODUCT: product[i].ID_PRODUCT,
                QUANTITY: quantity[i],
                NOTE: note[i]
            }, {transaction: t});
        }
    } else {
        await DETAIL_TRANSACTION.create({
            ID_TRANSACTION: idTransaction,
            ID_PRODUCT: product.ID_PRODUCT,
            QUANTITY: quantity,
            NOTE: note
        }, {transaction: t});
    }
}

function transactionResponse(transactions, res) {
    return res.status(OK).json({
        status: OK,
        message: getReasonPhrase(OK),
        transactions
    });
}

// noinspection JSCheckFunctionSignatures
class Transaction {
    async createTransaction(req, res) {
        const t = await sequelize.transaction();
        try {
            const {id_product, quantity, note, id_payment_method} = req.body;
            const {is_same_type, product} = await checkTransactionAndGetProduct(id_product, t);
            if (is_same_type) {
                const {total, sumQuantity} = calculateTotalAndQuantity(product, quantity);
                const createdTransaction = await TRANSACTION.create({
                    ID_PAYMENT_METHOD: id_payment_method,
                    TOTAL_QUANTITY: sumQuantity,
                    TOTAL_PRICE: total
                }, {transaction: t});
                await processDetailTransaction({
                    idTransaction: createdTransaction.ID_TRANSACTION,
                    product, quantity, note, t
                });
                await t.commit()
                createdTransaction.CREATED_AT = new Date().toISOString();
                return transactionResponse(createdTransaction, res);
            }
            await t.rollback();
            return transactionIsNotInTheSameType(res);
        } catch (e) {
            await t.rollback();
            return internalServerError(e, res);
        }
    }

    async readAllTransaction(req, res) {
        try {
            const {is_sell} = req.query;
            const {ID_ACCOUNT} = req.user;
            const transactions = await TRANSACTION.findAll({
                include: [
                    {
                        model: DETAIL_TRANSACTION,
                        required: true,
                        as: 'DETAIL_TRANSACTIONs',
                        include: {
                            model: PRODUCT,
                            required: true,
                            as: 'ID_PRODUCT_PRODUCT',
                            where: is_sell ? {IS_SELL: is_sell === 'true' ? 1 : 0} : {},
                            include: {
                                attributes: [],
                                model: BUSINESS,
                                required: true,
                                as: 'ID_BUSINESS_BUSINESS',
                                where: {ID_OWNER: ID_ACCOUNT}
                            }
                        }
                    },
                    {
                        model: PAYMENT_METHOD,
                        as: 'ID_PAYMENT_METHOD_PAYMENT_METHOD',
                        required: true
                    }
                ]
            });
            return transactionResponse(transactions, res);
        } catch (e) {
            return internalServerError(e, res);
        }
    }
}

module.exports = new Transaction();
