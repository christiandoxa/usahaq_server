const path = require('path');
const {notFound} = require("./common_response");
const {exist: {checkFileExist}} = require('../utils');
const cwd = path.join(__dirname, '..');

module.exports = function (req, res) {
    try {
        const {file_name} = req.query;
        const filePath = path.join(cwd, 'files', file_name);
        if (checkFileExist(filePath)) {
            return res.download(filePath);
        }
        return notFound(res);
    } catch (e) {
        return notFound(res);
    }
};
