const {NOT_FOUND, OK, getReasonPhrase} = require("http-status-codes");
const {PRODUCT} = require('../models');
const {handleDuplicateError, internalServerError} = require("./common_response");
const {message: {PRODUCT_NOT_FOUND}} = require('../constants');
const {
    file: {deleteLocalFile},
    cloudStorage: {cloudStorageUpload}
} = require('../utils');

function productResponse(product, res) {
    return res.status(OK).json({
        status: OK,
        message: getReasonPhrase(OK),
        product
    });
}

function productNotFound(res) {
    return res.status(NOT_FOUND).json({
        status: NOT_FOUND,
        message: PRODUCT_NOT_FOUND
    });
}

class Product {
    async createProduct(req, res) {
        const {file} = req;
        try {
            const {id_business, product_name, price, stock, is_sell} = req.body;
            const product = await PRODUCT.create({
                ID_BUSINESS: id_business,
                PRODUCT_NAME: product_name,
                PRICE: price,
                STOCK: stock,
                IS_SELL: is_sell,
                PRODUCT_IMAGE: file ? file.filename : null
            });
            await cloudStorageUpload(file, 'images');
            return productResponse(product, res);
        } catch (e) {
            deleteLocalFile(file);
            return handleDuplicateError(e, res);
        }
    }

    async updateProduct(req, res) {
        const {file} = req;
        try {
            const {id_business, id_product} = req.params;
            const {product_name, price, stock, is_sell} = req.body;
            const product = await PRODUCT.findOne({where: {ID_PRODUCT: id_product, ID_BUSINESS: id_business}});
            if (product) {
                const {dataValues: currentProduct} = product;
                if (file && file.filename && currentProduct.PRODUCT_IMAGE) {
                    deleteLocalFile(currentProduct.PRODUCT_IMAGE);
                }
                product.PRODUCT_NAME = product_name ?? currentProduct.PRODUCT_NAME;
                product.PRICE = price ?? currentProduct.PRICE;
                product.STOCK = stock ?? currentProduct.STOCK;
                product.IS_SELL = is_sell !== undefined ? is_sell : currentProduct.IS_SELL;
                product.PRODUCT_IMAGE = file ? file.filename : currentProduct.PRODUCT_IMAGE;
                const updatedProduct = await product.save();
                await cloudStorageUpload(file, 'images');
                return productResponse(updatedProduct, res);
            }
            return productNotFound(res);
        } catch (e) {
            deleteLocalFile(file);
            return handleDuplicateError(e, res);
        }
    }

    async readAllProduct(req, res) {
        try {
            const {id_business} = req.params;
            const products = await PRODUCT.findAll({where: {ID_BUSINESS: id_business}});
            return res.status(OK).json({
                status: OK,
                message: getReasonPhrase(OK),
                products
            });
        } catch (e) {
            return internalServerError(e, res);
        }
    }

    async readProduct(req, res) {
        try {
            const {id_business, id_product} = req.params;
            const product = await PRODUCT.findOne({where: {ID_BUSINESS: id_business, ID_PRODUCT: id_product}});
            if (product) {
                return productResponse(product, res);
            }
            return productNotFound(res);
        } catch (e) {
            return internalServerError(e, res);
        }
    }
}

module.exports = new Product();
