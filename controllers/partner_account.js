const {cloudStorageUpload} = require("../utils/cloud_storage");
const {handleDuplicateError, accountNotFound, accountResponse, internalServerError} = require("./common_response");
const {file: {deleteLocalFile}} = require("../utils");
const {ACCOUNT} = require('../models');

async function createAccountByEmailOrPhone(req) {
    const {file} = req;
    const {email, phone_number, user_id: firebase_id} = req.firebaseUser;
    const {
        name, identity_number, birth_date, address, custom_phone_number,
        custom_email, city, postal_code, username
    } = req.body;
    const baseAccount = {
        IDENTITY_NUMBER: identity_number,
        NAME: name,
        USERNAME: username,
        BIRTH_DATE: birth_date,
        ADDRESS: address,
        CITY: city,
        POSTAL_CODE: postal_code,
        ACCOUNT_IMAGE: file ? file.filename : null,
    }
    let createdAccount;
    if (email) {
        baseAccount.FIREBASE_EMAIL_ID = firebase_id;
        baseAccount.EMAIL = email;
        baseAccount.PHONE_NUMBER = custom_phone_number;
        createdAccount = await ACCOUNT.create(baseAccount);
    } else {
        baseAccount.FIREBASE_PHONE_ID = firebase_id;
        baseAccount.EMAIL = custom_email;
        baseAccount.PHONE_NUMBER = phone_number;
        createdAccount = await ACCOUNT.create(baseAccount);
    }
    createdAccount.LAST_LOGGED_IN = new Date().toISOString();
    return createdAccount;
}

class PartnerAccount {
    async createAccount(req, res) {
        const {file} = req;
        try {
            let createdAccount = await createAccountByEmailOrPhone(req);
            await cloudStorageUpload(file, 'images');
            return accountResponse(createdAccount, res);
        } catch (e) {
            deleteLocalFile(file);
            return handleDuplicateError(e, res);
        }
    }

    async updateAccount(req, res) {
        const {file} = req;
        try {
            const {ID_ACCOUNT} = req.user;
            const {name, identity_number, birth_date, address} = req.body;
            const account = await ACCOUNT.findOne({
                where: {ID_ACCOUNT}
            });
            if (account) {
                const {dataValues: currentAccount} = account;
                if (file && file.filename && currentAccount.ACCOUNT_IMAGE) {
                    deleteLocalFile(currentAccount.ACCOUNT_IMAGE);
                }
                account.NAME = name ?? currentAccount.NAME;
                account.IDENTITY_NUMBER = identity_number ?? currentAccount.IDENTITY_NUMBER;
                account.BIRTH_DATE = birth_date ?? currentAccount.BIRTH_DATE;
                account.ADDRESS = address ?? currentAccount.ADDRESS;
                account.ACCOUNT_IMAGE = file ? file.filename : currentAccount.ACCOUNT_IMAGE;
                const updatedAccount = await account.save();
                await cloudStorageUpload(file, 'images');
                return accountResponse(updatedAccount, res);
            }
            return accountNotFound(res);
        } catch (e) {
            deleteLocalFile(file);
            return handleDuplicateError(e, res);
        }
    }

    async signIn(req, res) {
        try {
            const {email, phone_number} = req.firebaseUser;
            let account;
            if (email) {
                account = await ACCOUNT.findOne({where: {EMAIL: email}});
            } else {
                account = await ACCOUNT.findOne({where: {PHONE_NUMBER: phone_number}});
            }
            if (account) {
                return accountResponse(account, res);
            }
            return accountNotFound(res);
        } catch (e) {
            return internalServerError(e, res);
        }
    }

    async readAccount(req, res) {
        try {
            const {ID_ACCOUNT} = req.user;
            const account = await ACCOUNT.findOne({where: {ID_ACCOUNT}});
            if (account) {
                return accountResponse(account, res);
            }
            return accountNotFound(res);
        } catch (e) {
            return internalServerError(e, res);
        }
    }
}

module.exports = new PartnerAccount();
