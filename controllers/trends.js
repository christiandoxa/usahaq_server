const {spawn} = require('child_process');
const {OK, INTERNAL_SERVER_ERROR, getReasonPhrase} = require('http-status-codes');
const {internalServerError} = require('./common_response');

function commonProcess(fileName, rawInput, res) {
    const input = Array.isArray(rawInput) ? rawInput : [rawInput];
    const python = spawn('python', [`./python_scripts/${fileName}`, ...input])
    let output;
    python.stdout.on('data', (data) => {
        output = data;
    });
    python.stderr.on('data', (data) => {
        output = data;
    });
    python.on('close', (code) => {
        const isSuccess = code === 0;
        const result = isSuccess ? JSON.parse(`${output}`.replace(/'/g, '"')) : output;
        return res.status(isSuccess ? OK : INTERNAL_SERVER_ERROR).json({
            status: isSuccess ? OK : INTERNAL_SERVER_ERROR,
            message: getReasonPhrase(isSuccess ? OK : INTERNAL_SERVER_ERROR),
            result: isSuccess ? result : null
        });
    });
}

class Trends {
    async getTrends(req, res) {
        try {
            const {keywords} = req.body;
            return commonProcess('gtrends.py', keywords, res);
        } catch (e) {
            return internalServerError(e, res);
        }
    }

    async getSuggestions(req, res) {
        try {
            return commonProcess('meatball.py', '', res);
        } catch (e) {
            return internalServerError(e, res);
        }
    }

    async getTodayTrends(req, res) {
        try {
            return commonProcess('today_trends.py', '', res);
        } catch (e) {
            return internalServerError(e, res);
        }
    }

    async getYearTrends(req, res) {
        try {
            const {year} = req.params;
            return commonProcess('year_trends.py', year, res);
        } catch (e) {
            return internalServerError(e, res);
        }
    }
}

module.exports = new Trends();
