const {OK, NOT_FOUND, getReasonPhrase} = require('http-status-codes');
const {PAYMENT_METHOD} = require('../models');
const {internalServerError} = require('./common_response');

class PaymentMethod {
    async getAllPaymentMethod(req, res) {
        try {
            const paymentMethod = await PAYMENT_METHOD.findAll();
            if (paymentMethod.length < 1) return res.status(NOT_FOUND).json({
                status: NOT_FOUND,
                message: getReasonPhrase(NOT_FOUND)
            });
            return res.status(OK).json({
                status: OK,
                message: getReasonPhrase(OK),
                paymentMethod
            })
        } catch (e) {
            return internalServerError(e, res);
        }
    }
}

module.exports = new PaymentMethod();
