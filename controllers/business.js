const {OK, NOT_FOUND, getReasonPhrase} = require("http-status-codes");
const {message: {BUSINESS_NOT_FOUND}} = require('../constants');
const {handleDuplicateError, internalServerError} = require("./common_response");
const {BUSINESS} = require('../models');
const {
    file: {deleteLocalFile},
    cloudStorage: {cloudStorageUpload}
} = require('../utils');

function businessResponse(business, res) {
    return res.status(OK).json({
        status: OK,
        message: getReasonPhrase(OK),
        business
    });
}

function businessNotFound(res) {
    return res.status(NOT_FOUND).json({
        status: NOT_FOUND,
        message: BUSINESS_NOT_FOUND
    });
}

class Business {
    async createBusiness(req, res) {
        const {file} = req;
        try {
            const {ID_ACCOUNT} = req.user;
            const {business_name, address, lat_lng} = req.body;
            const [latitude, longitude] = lat_lng;
            const createdBusiness = await BUSINESS.create({
                ID_OWNER: ID_ACCOUNT,
                BUSINESS_NAME: business_name,
                ADDRESS: address,
                LATITUDE: latitude,
                LONGITUDE: longitude,
                BUSINESS_IMAGE: file ? file.filename : null,
            });
            await cloudStorageUpload(file, 'images');
            return businessResponse(createdBusiness, res);
        } catch (e) {
            deleteLocalFile(file);
            return handleDuplicateError(e, res);
        }
    }

    async updateBusiness(req, res) {
        const {file} = req;
        try {
            const {ID_ACCOUNT} = req.user;
            const {id_business} = req.params;
            const {business_name, address, lat_lng} = req.body;
            const [latitude, longitude] = lat_lng;
            const business = await BUSINESS.findOne({where: {ID_OWNER: ID_ACCOUNT, ID_BUSINESS: id_business}});
            if (business) {
                const {dataValues: currentBusiness} = business;
                if (file && file.filename && currentBusiness.BUSINESS_IMAGE) {
                    deleteLocalFile(currentBusiness.BUSINESS_IMAGE);
                }
                business.BUSINESS_NAME = business_name ?? currentBusiness.BUSINESS_NAME;
                business.ADDRESS = address ?? currentBusiness.ADDRESS;
                business.LATITUDE = latitude ?? currentBusiness.LATITUDE;
                business.LONGITUDE = longitude ?? currentBusiness.LONGITUDE;
                business.BUSINESS_IMAGE = file ? file.filename : currentBusiness.BUSINESS_IMAGE;
                const updatedBusiness = await business.save();
                await cloudStorageUpload(file, 'images');
                return businessResponse(updatedBusiness, res);
            }
            return businessNotFound(res);
        } catch (e) {
            deleteLocalFile(file);
            return handleDuplicateError(e, res);
        }
    }

    async readAllBusiness(req, res) {
        try {
            const {ID_ACCOUNT} = req.user;
            const businesses = await BUSINESS.findAll({where: {ID_OWNER: ID_ACCOUNT}});
            return res.status(OK).json({
                status: OK,
                message: getReasonPhrase(OK),
                businesses
            });
        } catch (e) {
            return internalServerError(e, res);
        }
    }

    async readBusiness(req, res) {
        try {
            const {id_business} = req.params;
            const {ID_ACCOUNT} = req.user;
            const business = await BUSINESS.findOne({
                where: {
                    ID_OWNER: ID_ACCOUNT,
                    ID_BUSINESS: id_business
                }
            });
            if (business) {
                return businessResponse(business, res);
            }
            return businessNotFound(res);
        } catch (e) {
            return internalServerError(e, res);
        }
    }
}

module.exports = new Business();
