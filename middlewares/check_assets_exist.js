const {commonResponse: {notFound}} = require('../controllers');
const {cloudStorage: {cloudStorageDownload}} = require("../utils");

module.exports = async function (req, res, next) {
    try {
        const {folder, file_name} = req.query;
        await cloudStorageDownload(file_name, folder);
        next();
    } catch (e) {
        return notFound(res);
    }
};
