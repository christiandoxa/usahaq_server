const {jwt: {parseToken}} = require("../utils");

module.exports = function (socket, next) {
    const token = socket.handshake.auth.authorization;
    if (!token) next(new Error("Authorization token is required"));
    try {
        socket.handshake.auth.user = parseToken(token.split(" ")[1]);
        next();
    } catch (e) {
        next(e);
    }
}
