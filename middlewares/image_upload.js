const multer = require('multer');
const path = require('path');
const {BAD_REQUEST} = require("http-status-codes");
const {v4: uuidv4} = require('uuid');
const cwd = path.join(__dirname, '..');

const storage = multer.diskStorage({
    destination: path.join(cwd, 'files'),
    filename: function (_, file, cb) {
        cb(null, uuidv4() + path.extname(file.originalname));
    }
});

function checkFileType(file, cb) {
    const filetypes = /jpeg|jpg|png/;
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    const mimetype = filetypes.test(file.mimetype);

    if (mimetype && extname) {
        return cb(null, true);
    } else {
        cb('Error: Images Only!');
    }
}

// noinspection JSUnusedGlobalSymbols
const upload = multer({
    storage: storage,
    limits: {
        fieldNameSize: 100,
        fileSize: 1024 * 1024,
    },
    fileFilter: function (_, file, cb) {
        checkFileType(file, cb);
    }
}).single('image');


module.exports = function (req, res, next) {
    upload(req, res, function (err) {
        if (err) {
            return res.status(BAD_REQUEST).json({
                error: err.toString(),
                message: "Failed to process file upload"
            });
        }
        next();
    })
}
