const {PAYMENT_METHOD} = require('../models');

(async function () {
    try {
        console.log("Checking payment method data...");
        const count = await PAYMENT_METHOD.count();
        if (count > 0) {
            console.log("Payment data exist.");
        } else {
            console.log("Payment data isn't exist.");
            console.log("Seeding payment data...");
            const createdPaymentMethod = await PAYMENT_METHOD.bulkCreate([
                {PAYMENT_METHOD: 'Cash'},
                {PAYMENT_METHOD: 'Credit/Debt'},
                {PAYMENT_METHOD: 'E-wallet'},
            ]);
            console.log(createdPaymentMethod ? "Seeding success!" : "Seeding failed.")
        }
    } catch (e) {
        console.log("Failed to seed payment method data");
    } finally {
        console.log("Seeding checking done!");
    }
    process.exit();
})()
