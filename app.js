const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const app = express();
const httpServer = require("http").createServer(app);
const io = require("socket.io")(httpServer);
const helmet = require("helmet");
const mainRouter = require('./routes');
const websocketRouter = require('./websocket_routes');
const {checkWebsocketToken} = require('./middlewares');
const {string: {SOCKET_IO}} = require('./constants');
const {commonResponse: {notFound}} = require('./controllers');
const {cron} = require('./utils');

// run cron job
cron.start();

// websocket setup
io.use(checkWebsocketToken);
io.use(websocketRouter);
app.set(SOCKET_IO, io);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(helmet());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', mainRouter);
app.all('*', function (req, res, _) {
    return notFound(res);
});

if (cluster.isMaster) {
    console.log(`Primary ${process.pid} is running`);
    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
    cluster.on('exit', (worker) => {
        console.log(`worker ${worker.process.pid} died`);
    });
} else {
    // Workers can share any TCP connection
    // In this case it is an HTTP server
    const port = process.env.USAHAQ_PORT ?? 3000;
    httpServer.listen(parseInt(port));
    console.log(`Worker ${process.pid} started`);
}
