const express = require('express');
const router = express.Router();
const {transaction: {createTransaction, readAllTransaction}} = require('../controllers');
const {transactionValidator: {createTransactionValidator, getAllTransactionValidator}} = require('../validators');

router.get('/', getAllTransactionValidator, readAllTransaction);
router.post('/create', createTransactionValidator, createTransaction);

module.exports = router;
