const express = require('express');
const router = express.Router();
const fileServing = require('./file_serving');
const partnerAccount = require('./partner_account');
const business = require('./business');
const product = require('./product');
const machineLearning = require('./machine_learning');
const trends = require('./trends');
const transaction = require('./transaction');
const paymentMethod = require('./payment_method');
const {
    customValidator: {
        checkToken,
        checkAssetToken
    }
} = require("../validators");

router.use('/partner/account', partnerAccount);
router.use('/business', checkToken, business);
router.use('/product', checkToken, product);
router.use('/assets', checkAssetToken, fileServing);
router.use('/machine_learning', checkToken, machineLearning);
router.use('/trends', checkToken, trends);
router.use('/transaction', checkToken, transaction);
router.use('/payment_method', checkToken, paymentMethod);

module.exports = router;
