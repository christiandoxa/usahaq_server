const express = require('express');
const {fileServing} = require('../controllers');
const {checkAssetsExist} = require('../middlewares');
const router = express.Router();

router.get('/', checkAssetsExist, fileServing);

module.exports = router;
