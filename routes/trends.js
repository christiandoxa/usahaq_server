const express = require('express');
const {trends: {getTrends, getTodayTrends, getYearTrends, getSuggestions}} = require('../controllers');
const {trendsValidator: {getTrendsValidator, getYearTrendsValidator}} = require('../validators');
const router = express.Router();

router.get('/year/:year', getYearTrendsValidator, getYearTrends);
router.get('/suggestions', getSuggestions);
router.get('/today', getTodayTrends);
router.post('/', getTrendsValidator, getTrends);

module.exports = router;
