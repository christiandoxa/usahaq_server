const express = require('express');
const router = express.Router();
const {imageUpload} = require("../middlewares");
const {
    partnerAccount: {
        readAccount,
        signIn,
        createAccount,
        updateAccount
    }
} = require("../controllers");
const {
    customValidator: {
        checkToken,
        checkFirebaseToken
    },
    partnerAccountValidator: {
        createAccountValidator,
        updateAccountValidator
    },
} = require("../validators");

router.get('/', checkToken, readAccount);
router.post('/sign_in', checkFirebaseToken, signIn)
router.post('/create', ...checkFirebaseToken, imageUpload, createAccountValidator, createAccount);
router.put('/update', checkToken, imageUpload, updateAccountValidator, updateAccount);

module.exports = router;
