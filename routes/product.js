const express = require('express');
const {imageUpload} = require("../middlewares");
const {
    product: {
        createProduct,
        updateProduct,
        readAllProduct,
        readProduct
    }
} = require('../controllers');
const {
    customValidator: {
        checkBusinessId
    },
    productValidator: {
        readProductValidation,
        createProductValidation,
        updateProductValidation
    }
} = require('../validators');
const router = express.Router();

router.get('/:id_business', checkBusinessId, readAllProduct);
router.get('/:id_business/:id_product', readProductValidation, readProduct);
router.post('/create', imageUpload, createProductValidation, createProduct);
router.put('/:id_business/:id_product/update', imageUpload, updateProductValidation, updateProduct);

module.exports = router;
