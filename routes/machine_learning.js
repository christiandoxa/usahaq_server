const express = require('express');
const {
    machineLearningValidator: {checkPayloadAndParam, checkParam}
} = require('../validators');
const {
    machineLearning: {
        currentPrediction,
        getPrediction
    }
} = require('../controllers');
const router = express.Router();

router.get('/:param_type', checkParam, currentPrediction);
router.post('/:param_type', checkPayloadAndParam, getPrediction);

module.exports = router;
