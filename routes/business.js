const express = require('express');
const {imageUpload} = require("../middlewares");
const {
    business: {
        readAllBusiness,
        createBusiness,
        updateBusiness,
        readBusiness
    }
} = require('../controllers');
const {
    businessValidator: {
        createBusinessValidator,
        updateBusinessValidator,
        getBusinessValidator
    }
} = require("../validators");
const router = express.Router();

router.get('/', readAllBusiness);
router.get('/:id_business', getBusinessValidator, readBusiness);
router.post('/create', imageUpload, createBusinessValidator, createBusiness);
router.put('/:id_business/update', imageUpload, updateBusinessValidator, updateBusiness);

module.exports = router;
