const express = require('express');
const router = express.Router();
const {paymentMethod: {getAllPaymentMethod}} = require('../controllers');

router.get('/', getAllPaymentMethod);

module.exports = router;
