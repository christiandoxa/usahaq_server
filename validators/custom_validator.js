const {header, query, param} = require('express-validator');
const {BUSINESS} = require('../models');
const {admin} = require("../keys");
const validationErrorResponse = require("./validation_response");
const {UNAUTHORIZED} = require("http-status-codes");
const {
    calculateAge,
    jwt: {parseToken},
} = require("../utils");
const {validationMessage: {NOT_FOUND_OR_UNAUTHORIZED}} = require('../constants');
const {
    validationMessage: {AUTHORIZATION_TOKEN_REQUIRED, ONLY_WORD},
    regex: {WORD_REGEX}
} = require("../constants");


async function validateFirebaseToken(token, req) {
    try {
        if (!token) {
            req.status = UNAUTHORIZED;
            return Promise.reject("Token header is required");
        }
        req.firebaseUser = await admin.auth().verifyIdToken(token.split(" ")[1]);
        return Promise.resolve();
    } catch (e) {
        req.status = UNAUTHORIZED;
        return Promise.reject(e);
    }
}

function validateToken(token, req) {
    if (!token) {
        req.status = UNAUTHORIZED;
        throw new Error("Token header is required");
    }
    try {
        req.user = parseToken(token.split(" ")[1]);
        return true;
    } catch (e) {
        req.status = UNAUTHORIZED;
        throw e;
    }
}

class CustomValidator {
    static checkBusinessId = [
        param('id_business').custom(this.validateIdBusiness),
        validationErrorResponse
    ];

    static checkFirebaseToken = [
        header("authorization", AUTHORIZATION_TOKEN_REQUIRED).custom(function (token, {req}) {
            return validateFirebaseToken(token, req);
        }),
        validationErrorResponse
    ];

    static checkToken = [
        header("authorization", AUTHORIZATION_TOKEN_REQUIRED).custom(function (token, {req}) {
            return validateToken(token, req);
        }),
        validationErrorResponse
    ];

    static checkAssetToken = [
        query("authorization", AUTHORIZATION_TOKEN_REQUIRED).custom(function (token, {req}) {
            return validateToken(token, req);
        }),
        validationErrorResponse
    ];

    static validateAge(date) {
        if (!date) throw new Error("Date required");
        if (calculateAge(date) < 15) {
            throw new Error("Age need to be at least 15 years old.");
        }
        return true;
    }

    static async validateIdBusiness(id_business, {req}) {
        try {
            if (!id_business) return Promise.reject("Id business is required");
            const {ID_ACCOUNT} = req.user;
            const result = await BUSINESS.findOne({
                attributes: ['ID_BUSINESS'],
                where: {ID_BUSINESS: id_business, ID_OWNER: ID_ACCOUNT}
            });
            if (result) {
                return Promise.resolve();
            } else {
                return Promise.reject(NOT_FOUND_OR_UNAUTHORIZED);
            }
        } catch (e) {
            return Promise.reject(e);
        }
    }

    static validateInputIsWord(values) {
        return new Promise(function (resolve, reject) {
            if (!values) reject("value is required");
            if (Array.isArray(values)) {
                values.map(function (value) {
                    if (!WORD_REGEX.test(value)) reject(ONLY_WORD)
                });
            } else {
                if (!WORD_REGEX.test(values)) reject(ONLY_WORD)
            }
            resolve();
        })
    }
}

module.exports = CustomValidator;
