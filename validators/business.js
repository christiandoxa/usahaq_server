const {body, param, oneOf} = require('express-validator');
const validationErrorResponse = require("./validation_response");
const {validateIdBusiness} = require('./custom_validator');
const {
    regex: {
        BUSINESS_NAME_REGEX,
        ADDRESS_REGEX
    }
} = require("../constants");

const baseBusinessValidator = [
    body("business_name").isLength({min: 5, max: 1000}).matches(BUSINESS_NAME_REGEX).trim(),
    body("address").isLength({min: 10, max: 1000}).matches(ADDRESS_REGEX).trim(),
    body("lat_lng").isLatLong().customSanitizer(function (value) {
        if (!value) return value;
        return value.replace(/\s/, "").split(",");
    }),
]

exports.createBusinessValidator = [
    ...baseBusinessValidator,
    validationErrorResponse
];

exports.updateBusinessValidator = [
    param("id_business").isNumeric().custom(validateIdBusiness),
    oneOf([...baseBusinessValidator]),
    validationErrorResponse
];

exports.getBusinessValidator = [
    param("id_business").isNumeric().custom(validateIdBusiness),
    validationErrorResponse
];
