const {body, param, oneOf} = require('express-validator');
const validationErrorResponse = require("./validation_response");
const {validateIdBusiness} = require('./custom_validator');
const {
    regex: {
        PRODUCT_NAME_REGEX
    }
} = require("../constants");


const baseProductValidation = [
    body('product_name').matches(PRODUCT_NAME_REGEX),
    body('price').isInt({min: 0}),
    body('stock').isInt({min: 0}),
    body('is_sell').isBoolean(),
];

const baseParamValidation = [
    param('id_business').isNumeric().custom(validateIdBusiness),
    param("id_product").isNumeric()
];

exports.readProductValidation = [
    ...baseParamValidation,
    validationErrorResponse
];

exports.createProductValidation = [
    body('id_business').isNumeric().custom(validateIdBusiness),
    ...baseProductValidation,
    validationErrorResponse
];

exports.updateProductValidation = [
    ...baseParamValidation,
    oneOf([...baseProductValidation]),
    validationErrorResponse
];
