const {body, query} = require('express-validator');
const validationErrorResponse = require("./validation_response");

exports.createTransactionValidator = [
    body('id_product').isInt({min: 1}),
    body('id_payment_method').isInt({min: 1}),
    body('quantity').isInt({min: 1}),
    body('note').exists(),
    validationErrorResponse
];

exports.getAllTransactionValidator = [
    query('is_sell').isBoolean().optional(),
    validationErrorResponse
];
