const {body, oneOf} = require('express-validator');
const {validateAge} = require("./custom_validator");
const validationErrorResponse = require("./validation_response");
const {regex: {PERSON_NAME_REGEX, ADDRESS_REGEX}} = require("../constants");

const baseAccountValidator = [
    body("identity_number").isNumeric().isLength({min: 10, max: 20}),
    body("name").isLength({min: 3, max: 100}).matches(PERSON_NAME_REGEX).trim(),
    body("address").isLength({min: 10, max: 1000}).matches(ADDRESS_REGEX).trim(),
    body("birth_date").isDate().custom(validateAge),
    body('city').isAlpha().isLength({max: 30}),
    body('postal_code').isInt().isLength({min: 5, max: 10}),
    body('username').isAlphanumeric().isLength({min: 5, max: 30}),
    body('custom_phone_number').isMobilePhone(null, {strictMode: true}).optional(),
    body('custom_email').isEmail().optional()
];

exports.createAccountValidator = [
    ...baseAccountValidator,
    validationErrorResponse
];

exports.updateAccountValidator = [
    oneOf([...baseAccountValidator]),
    validationErrorResponse
];
