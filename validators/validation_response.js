const {validationResult} = require('express-validator');
const {BAD_REQUEST, getReasonPhrase} = require('http-status-codes');

module.exports = function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(req.status ?? BAD_REQUEST).json({
            status: req.status ?? BAD_REQUEST,
            message: req.status ? getReasonPhrase(req.status) : getReasonPhrase(BAD_REQUEST),
            errors: errors.array()
        });
    }
    next();
}
