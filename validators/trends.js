const {body, param} = require('express-validator');
const validationErrorResponse = require("./validation_response");
const {validateInputIsWord} = require('./custom_validator');

exports.getTrendsValidator = [
    body('keywords').exists().custom(validateInputIsWord),
    validationErrorResponse
];

exports.getYearTrendsValidator = [
    param('year').isInt().custom(function (value) {
        if (!value) throw new Error("year is required");
        if (parseInt(value) < new Date().getFullYear()) {
            return true;
        }
        throw new Error("current year is not supported");
    }),
    validationErrorResponse
];
