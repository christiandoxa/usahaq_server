const {body, param} = require('express-validator');
const validationErrorResponse = require("./validation_response");
const {regex: {PARAM_REGEX}} = require('../constants');

const baseValidation = [
    param('param_type').matches(PARAM_REGEX),
];

exports.checkParam = [
    ...baseValidation,
    validationErrorResponse
];

exports.checkPayloadAndParam = [
    ...baseValidation,
    body('payload').isArray(),
    validationErrorResponse
];
