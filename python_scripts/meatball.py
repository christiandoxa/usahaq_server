from pytrends.request import TrendReq

pytrends = TrendReq(hl='en-US', tz=360)
pytrends.build_payload(['bakso'], timeframe='today 12-m', geo='ID')
rel_top = pytrends.related_topics()
rel_top = rel_top['bakso']['top']
rel_top = rel_top.drop(['value', 'formattedValue', 'topic_type', 'hasData', 'topic_mid'], 1)
final_res = {}
base_url = 'https://trends.google.co.id'
for column in range(len(rel_top)):
    final_res[rel_top.iloc[column, 1]] = "{}{}".format(base_url, rel_top.iloc[column, 0])
print(final_res)