from pytrends.request import TrendReq
import sys
from datetime import datetime

pytrends = TrendReq(hl='en-US', tz=360)
kw_list = sys.argv[1:]
pytrends.build_payload(kw_list, timeframe='today 12-m', geo='ID')
data = pytrends.interest_over_time()
data = data.drop('isPartial',1)
date = []
for column in data[kw_list[0]].keys().to_list():
    date_temp = datetime.strftime(column, "%Y-%m-%d")
    date.append(date_temp)
interest = {}
temp = []
for i in date:
    for j in data.columns:
        temp.append(data[j][i])
    interest[i]=temp
    temp=[]

rel_quer = pytrends.related_queries()
related = {}
query = rel_quer
for i in rel_quer:
    rel = {}
    for j in range(len(rel_quer[i]['top']['query'])):
        rel[rel_quer[i]['top']['query'][j]] = rel_quer[i]['top']['value'][j]
        related[i] = rel
reg_quer = pytrends.interest_by_region(resolution='COUNTRY', inc_low_vol=True, inc_geo_code=False)
region = {}
reg = {}
for i in reg_quer:
    for j in reg_quer[kw_list[0]].keys().to_list():
        reg[j] = reg_quer.loc[j,i]
        region[i] = reg
    reg = {}
final_res = {
    'interest_over_time': interest,
    'related_queries': related,
    'interest_by_region': region
}
print(final_res)
