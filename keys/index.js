const admin = require("firebase-admin");
const serviceAccount = require("./usahaq-adminsdk.json");

exports.admin = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    storageBucket: "usahaq-id.appspot.com"
});

exports.jwtKey = "10fa61cc^7e27&5ab9-ba7c#c9cb48b06459";
