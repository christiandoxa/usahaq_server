module.exports = {
    ADDRESS_REGEX: /^[a-zA-Z0-9\s,'-\\.]*$/,
    BUSINESS_NAME_REGEX: /^[a-z\d\s]+$/i,
    PARAM_REGEX: /^[a-z_]+$/i,
    PERSON_NAME_REGEX: /^[a-z\s]+$/i,
    PRODUCT_NAME_REGEX: /^[a-z\d\s]+$/i,
    WORD_REGEX: /^[A-Za-z\s]+$/
};
