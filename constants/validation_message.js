module.exports = {
    AUTHORIZATION_TOKEN_REQUIRED: "Authorization token required",
    NOT_FOUND_OR_UNAUTHORIZED: "Not found or unauthorized to process this entity",
    ONLY_WORD: "only word is allowed"
};
