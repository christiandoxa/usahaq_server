module.exports = {
    ACCOUNT_NOT_FOUND: "Can't find account with provided token",
    BUSINESS_NOT_FOUND: "Can't find business with provided token",
    PRODUCT_NOT_FOUND: "Can't find product with provided information",
}
