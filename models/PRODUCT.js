const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('PRODUCT', {
        ID_PRODUCT: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true
        },
        ID_BUSINESS: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'BUSINESS',
                key: 'ID_BUSINESS'
            }
        },
        PRODUCT_NAME: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        PRODUCT_IMAGE: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        PRICE: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false,
            defaultValue: 0.00
        },
        STOCK: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        IS_SELL: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: 0
        }
    }, {
        sequelize,
        tableName: 'PRODUCT',
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "ID_PRODUCT"},
                ]
            },
            {
                name: "PRODUCT_ak_1",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "ID_BUSINESS"},
                    {name: "PRODUCT_NAME"},
                ]
            },
        ]
    });
};
