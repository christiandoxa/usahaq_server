const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('DETAIL_TRANSACTION', {
        ID_TRANSACTION: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'TRANSACTION',
                key: 'ID_TRANSACTION'
            }
        },
        ID_PRODUCT: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PRODUCT',
                key: 'ID_PRODUCT'
            }
        },
        QUANTITY: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        NOTE: {
            type: DataTypes.STRING(255),
            allowNull: true
        }
    }, {
        sequelize,
        tableName: 'DETAIL_TRANSACTION',
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "ID_TRANSACTION"},
                    {name: "ID_PRODUCT"},
                ]
            },
            {
                name: "DETAIL_SELL_TRANSACTION_PRODUCT",
                using: "BTREE",
                fields: [
                    {name: "ID_PRODUCT"},
                ]
            },
        ]
    });
};
