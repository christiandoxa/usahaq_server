const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('BUSINESS', {
        ID_BUSINESS: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true
        },
        ID_OWNER: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'ACCOUNT',
                key: 'ID_ACCOUNT'
            }
        },
        BUSINESS_NAME: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        BUSINESS_IMAGE: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        ADDRESS: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        LATITUDE: {
            type: DataTypes.DECIMAL(10, 8),
            allowNull: false
        },
        LONGITUDE: {
            type: DataTypes.DECIMAL(11, 8),
            allowNull: false
        }
    }, {
        sequelize,
        tableName: 'BUSINESS',
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "ID_BUSINESS"},
                ]
            },
            {
                name: "BUSINESS_ak_1",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "ID_OWNER"},
                    {name: "BUSINESS_NAME"},
                ]
            },
        ]
    });
};
