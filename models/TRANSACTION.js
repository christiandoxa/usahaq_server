const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('TRANSACTION', {
        ID_TRANSACTION: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true
        },
        ID_PAYMENT_METHOD: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'PAYMENT_METHOD',
                key: 'ID_PAYMENT_METHOD'
            }
        },
        TOTAL_QUANTITY: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        TOTAL_PRICE: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false
        },
        CREATED_AT: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
        }
    }, {
        sequelize,
        tableName: 'TRANSACTION',
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "ID_TRANSACTION"},
                ]
            },
            {
                name: "SELL_TRANSACTION_PAYMENT_METHOD",
                using: "BTREE",
                fields: [
                    {name: "ID_PAYMENT_METHOD"},
                ]
            },
        ]
    });
};
