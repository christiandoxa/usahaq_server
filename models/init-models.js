var DataTypes = require("sequelize").DataTypes;
var _ACCOUNT = require("./ACCOUNT");
var _BUSINESS = require("./BUSINESS");
var _DETAIL_TRANSACTION = require("./DETAIL_TRANSACTION");
var _PAYMENT_METHOD = require("./PAYMENT_METHOD");
var _PRODUCT = require("./PRODUCT");
var _TRANSACTION = require("./TRANSACTION");

function initModels(sequelize) {
    var ACCOUNT = _ACCOUNT(sequelize, DataTypes);
    var BUSINESS = _BUSINESS(sequelize, DataTypes);
    var DETAIL_TRANSACTION = _DETAIL_TRANSACTION(sequelize, DataTypes);
    var PAYMENT_METHOD = _PAYMENT_METHOD(sequelize, DataTypes);
    var PRODUCT = _PRODUCT(sequelize, DataTypes);
    var TRANSACTION = _TRANSACTION(sequelize, DataTypes);

    PRODUCT.belongsToMany(TRANSACTION, {
        as: 'ID_TRANSACTION_TRANSACTIONs',
        through: DETAIL_TRANSACTION,
        foreignKey: "ID_PRODUCT",
        otherKey: "ID_TRANSACTION"
    });
    TRANSACTION.belongsToMany(PRODUCT, {
        as: 'ID_PRODUCT_PRODUCTs',
        through: DETAIL_TRANSACTION,
        foreignKey: "ID_TRANSACTION",
        otherKey: "ID_PRODUCT"
    });
    BUSINESS.belongsTo(ACCOUNT, {as: "ID_OWNER_ACCOUNT", foreignKey: "ID_OWNER"});
    ACCOUNT.hasMany(BUSINESS, {as: "BUSINESSes", foreignKey: "ID_OWNER"});
    PRODUCT.belongsTo(BUSINESS, {as: "ID_BUSINESS_BUSINESS", foreignKey: "ID_BUSINESS"});
    BUSINESS.hasMany(PRODUCT, {as: "PRODUCTs", foreignKey: "ID_BUSINESS"});
    TRANSACTION.belongsTo(PAYMENT_METHOD, {as: "ID_PAYMENT_METHOD_PAYMENT_METHOD", foreignKey: "ID_PAYMENT_METHOD"});
    PAYMENT_METHOD.hasMany(TRANSACTION, {as: "TRANSACTIONs", foreignKey: "ID_PAYMENT_METHOD"});
    DETAIL_TRANSACTION.belongsTo(PRODUCT, {as: "ID_PRODUCT_PRODUCT", foreignKey: "ID_PRODUCT"});
    PRODUCT.hasMany(DETAIL_TRANSACTION, {as: "DETAIL_TRANSACTIONs", foreignKey: "ID_PRODUCT"});
    DETAIL_TRANSACTION.belongsTo(TRANSACTION, {as: "ID_TRANSACTION_TRANSACTION", foreignKey: "ID_TRANSACTION"});
    TRANSACTION.hasMany(DETAIL_TRANSACTION, {as: "DETAIL_TRANSACTIONs", foreignKey: "ID_TRANSACTION"});

    return {
        ACCOUNT,
        BUSINESS,
        DETAIL_TRANSACTION,
        PAYMENT_METHOD,
        PRODUCT,
        TRANSACTION,
    };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
