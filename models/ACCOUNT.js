const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('ACCOUNT', {
        ID_ACCOUNT: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true
        },
        FIREBASE_PHONE_ID: {
            type: DataTypes.STRING(50),
            allowNull: true,
            unique: "ACCOUNT_ak_4"
        },
        FIREBASE_EMAIL_ID: {
            type: DataTypes.STRING(50),
            allowNull: true,
            unique: "ACCOUNT_ak_5"
        },
        IDENTITY_NUMBER: {
            type: DataTypes.STRING(20),
            allowNull: false,
            unique: "ACCOUNT_ak_3"
        },
        NAME: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        USERNAME: {
            type: DataTypes.STRING(30),
            allowNull: false,
            unique: "ACCOUNT_ak_6"
        },
        PHONE_NUMBER: {
            type: DataTypes.STRING(15),
            allowNull: true,
            unique: "ACCOUNT_ak_1"
        },
        EMAIL: {
            type: DataTypes.STRING(100),
            allowNull: true,
            unique: "ACCOUNT_ak_2"
        },
        BIRTH_DATE: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        ADDRESS: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        CITY: {
            type: DataTypes.STRING(30),
            allowNull: false
        },
        POSTAL_CODE: {
            type: DataTypes.STRING(10),
            allowNull: false
        },
        ACCOUNT_IMAGE: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        LAST_LOGGED_IN: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
        }
    }, {
        sequelize,
        tableName: 'ACCOUNT',
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "ID_ACCOUNT"},
                ]
            },
            {
                name: "ACCOUNT_ak_3",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "IDENTITY_NUMBER"},
                ]
            },
            {
                name: "ACCOUNT_ak_6",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "USERNAME"},
                ]
            },
            {
                name: "ACCOUNT_ak_1",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "PHONE_NUMBER"},
                ]
            },
            {
                name: "ACCOUNT_ak_2",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "EMAIL"},
                ]
            },
            {
                name: "ACCOUNT_ak_4",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "FIREBASE_PHONE_ID"},
                ]
            },
            {
                name: "ACCOUNT_ak_5",
                unique: true,
                using: "BTREE",
                fields: [
                    {name: "FIREBASE_EMAIL_ID"},
                ]
            },
        ]
    });
};
