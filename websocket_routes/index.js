module.exports = function (socket, next) {
    const {request} = socket.handshake.query;
    socket.emit(request, `this is your request ${request}`);
    next();
}
